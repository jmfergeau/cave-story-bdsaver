# Cave Story screensaver for Before Dawn
This is a web screensaver based on the game Cave Story and using the (deprecated) jQuery Spritely library.

- Sprites from [Studio Pixel](http://studiopixel.sakura.ne.jp/doukutsumonogatari/)
- [Spritely's jQuery library](http://spritely.net/)

You can check out a sample of it below:
- [with Balrog Curly and Quote (default)](https://jmfergeau.gitlab.io/cave-story-bdsaver/)
- [with a dragon, Kazuma and Quote](https://jmfergeau.gitlab.io/cave-story-bdsaver/index.html?sprite=kazuma)

## Using it with Before Dawn
This screensaver is already bundled in Before Dawn! Just select "Cave Story" in the program's window.
